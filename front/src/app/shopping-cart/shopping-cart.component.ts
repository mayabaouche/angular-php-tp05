import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';
import { DelProduct } from '../common/actions/delProduct.action';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  quantity : number;
  articles : Observable<Product>;
  isEmpty : Boolean = true;
  
  constructor(private store: Store) {
    this.store.select(state => state.shoppingCart.shoppingCart).subscribe (u => this.quantity = u.length);
    this.store.select(state => state.shoppingCart.shoppingCart).subscribe (u => this.isEmpty = u.length == 0);
    this.articles = this.store.select(state => state.shoppingCart.shoppingCart);
   }

  ngOnInit() {
  }

  onDelClick(article) { 
    this.delProduct(article);
  }

  delProduct(article) { 
    this.store.dispatch(new DelProduct(article)); 
  }

}