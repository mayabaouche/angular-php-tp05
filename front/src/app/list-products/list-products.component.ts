import { Component, OnInit , EventEmitter, Input, Output} from '@angular/core';
import { Product } from '../models/product.model';
import { Store } from '@ngxs/store';
import { AddProduct } from '../common/actions/addProduct.action';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  
  public products : Product[] = [];
  private filteredProducts : Product[] = [];
  public isEmpty : boolean = false;
  private quantity : number = 0; 
  @Input() filterContent : String; 
  @Output() qty: EventEmitter<number> = new EventEmitter<number>();

  constructor(public listProductService: ApiService, private store: Store) { }
  ngOnInit() 
  {
    this.listProductService.getProducts().subscribe(response => {
      this.products = response;
      this.filteredProducts = response;
    }); 
  }

  searchProductByName(filter : string)
  {
    this.filterContent = filter;
    this.isEmpty = false;
    this.filteredProducts = [];
    if (this.filterContent === ""){
      this.filteredProducts = this.products;
      return; 
    }
    for (let i=0; i < this.products.length; i++)
    {
        if (this.products[i].productName.toLowerCase().includes(this.filterContent.toLowerCase().trim()))
        {
          this.filteredProducts.push(this.products[i]);
        }
    }
    if (this.filteredProducts.length == 0)
      this.isEmpty = true;
  }

   onAddClick(p )  {
    this.addProduct(p);
  }
  
  addProduct(p) { 
    this.quantity ++;
    this.store.dispatch(new AddProduct(p)); 
    this.qty.emit(this.quantity);
  }
}
