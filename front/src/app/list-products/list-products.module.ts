import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ListProductsComponent } from './list-products.component';
import { FilterProductsComponent} from './filter-products/filter-products.component';
import { RouterModule, Routes } from '@angular/router';

export const productsRoutes:Routes = [
    
        { 
          path: '',
          component: ListProductsComponent 
        }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild(productsRoutes)
  ],
  declarations: [ ListProductsComponent, FilterProductsComponent], 
  exports: [RouterModule]
})
export class ListProductsModule { }
