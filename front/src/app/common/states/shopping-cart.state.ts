import {Action,Selector,State, StateContext} from '@ngxs/store';
import {ShoppingCartStateModel} from './shopping-cart-model.state';
import {AddProduct} from '../actions/addProduct.action';
import {DelProduct} from '../actions/delProduct.action';

@State<ShoppingCartStateModel>({
    name: 'shoppingCart',
    defaults: {
        shoppingCart: []
    }
})

export class ShoppingCartState {

  @Selector()
    static getShoppingCart(state: ShoppingCartStateModel) {
        return state.shoppingCart;
    }

@Action(AddProduct)
    add({getState, patchState }: StateContext<ShoppingCartStateModel>, { payload }: AddProduct) {
        const state = getState();
        patchState({
            shoppingCart: [...state.shoppingCart, payload]
        });
    }

 @Action(DelProduct)
    del ({getState, patchState }: StateContext<ShoppingCartStateModel>, { payload }: AddProduct) {
        const state = getState();
        let index = state.shoppingCart.indexOf(payload);
        state.shoppingCart.splice(index,1);
        patchState({
            shoppingCart: [...(state.shoppingCart)]
        });
    }   
}