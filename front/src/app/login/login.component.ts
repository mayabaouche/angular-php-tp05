import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: FormGroup;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.credentials = new FormGroup({
      username: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')
      ]))
   });
  }

  setSession(token) {
    localStorage.setItem('token', token);
  }

  login()
  {
    this.apiService.login(this.credentials.controls['username'].value, this.credentials.controls['password'].value)
      .subscribe(
        response => this.setSession(response)
      ); 
  }

}
