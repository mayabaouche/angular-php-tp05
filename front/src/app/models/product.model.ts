export class Product {
  public id : number = 0;
  public productName : String = ''; 
  public productPrice : number = 0; 
  public manufactor : string = '';
  public productImg : string = '';
  public description : string = ''; 
}