export const environment = {
  production: false,
  backendClient: 'http://node12.codenvy.io:37124/back/index.php/client', 
  backendProduct: 'http://node12.codenvy.io:37124/back/index.php/products',
  productById: 'http://node12.codenvy.io:37124/back/index.php/product',
  login: 'http://node12.codenvy.io:37124/back/index.php/login'
};
