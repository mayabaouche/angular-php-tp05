<?php

class Client
{
    public $id;
    public $gender;
    public $first_name;
    public $last_name;
    public $adress;
    public $zip_code; 
    public $city; 
    public $country; 
    public $phone;    
    public $mail; 
    public $login; 
    public $password; 
    
    function __construct($id, $gender, $firstName, $lastName, $adress, $zipCode, $city, $country, $phone, $mail, $login, $password)
    {
        $this->id = $id;
        $this->gender = $gender; 
        $this->first_name = $firstName;
        $this->last_name = $lastName;
        $this->adress =  $adress; 
        $this->zip_code = $zipCode; 
        $this->city = $city; 
        $this->country = $country; 
        $this->phone = $phone;    
        $this->mail = $mail; 
        $this->login = $mail; 
        $this->password = $password; 
    }
    
    // setters/getters
    function set_lastName($name) 
    {
        $this->last_name = $name;
    }
    
    function get_lastName() 
    {
        return $this->last_name;
    }
    
    function set_firstName($name) 
    {
        $this->first_name = $name;
    }
    
    function get_firstName() 
    {
        return $this->first_name;
    }
    
    function set_mail($mail) 
    {
        $this->mail = $mail;
    }
    
    function get_mail() 
    {
        return $this->mail;
    }
        
    function set_password($password) 
    {
        $this->password = $password;
    }
    
    function get_password() 
    {
        return $this->password;
    }
}
?>