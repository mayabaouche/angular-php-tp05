<?php
use \Firebase\JWT\JWT;

require 'vendor/autoload.php';
include_once 'controllers/ClientController.php'; 
include_once './models/Client.php'; 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Access-Control-Allow-Origin: *"); 
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, X-Auth-Token, content-type');

const JWT_SECRET = "makey1234567";

$jwt = new \Slim\Middleware\JwtAuthentication([
"path" => "/api",
"secure" => false,
"secret" => JWT_SECRET,
"passthrough" => ["/login"],
"attribute" => "decoded_token_data",
"algorithm" => ["HS256"],
"error" => function ($response, $arguments) {    
    $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
    return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

function addClient($request, $response, $args)
{
    $body = $request->getParsedBody();
    $gender = $body['gender'];
    $firstName = $body['first_name'];
    $lastName = $body['last_name'];
    $adress = $body['adress'];   
    $zipCode = $body['zip_code'];
    $city = $body['city'];
    $country = $body['country'];
    $phone = $body['phone']; 
    $mail = $body['mail']; 
    $login = $body['login']; 
    $password = hash('sha256', $body['password']); 
       
    $file = 'res/Client.json'; 
    
    $data[] = [
        'gender' => $gender,
        'first_name' => $firstName, 
        'last_name' => $lastName, 
        'adress' => $adress, 
        'zip_code' => $zipCode,
        'city' => $city, 
        'country' => $country, 
        'phone' => $phone,
        'mail' => $mail, 
        'login' => $login,
        'password' => $password
    ]; 
    
    $inp = file_get_contents($file, true);   
        
    $tempArray = json_decode($inp);
    
    array_push($tempArray, $data);
    
    $jsonData = json_encode($tempArray);
        
    file_put_contents($file, $jsonData);
    
    $c = new Client("0", $gender, $firstName, $lastName, $adress, $zipCode, $city, $country, $phone, $mail, $login, $password);
    
    return $response->write(json_encode($c)); 
}

function getProductById($request, $response, $args)
{ 
    $id = $args['id'];       
    $file = 'res/products.json'; 
    $json = json_decode(file_get_contents($file));
    foreach ($json as $item) {
        if ($item->id == $id) {
          return $response->write(json_encode($item)); 
        }
    } 
    return $response->write("Product not found.");
}

function getProducts($request, $response, $args)
{          
    $file = 'res/products.json'; 
    return $response->write(file_get_contents($file)); 
}

function login($request, $response, $args) {
    
    $body = $request->getParsedBody();
    $login = $body['login'];
    $password = $body['password'];
    $issuedAt = time();
    $expirationTime = $issuedAt + 60; // jwt valid for 60 seconds from the issued time
    $payload = array(
    'login' => $login,
    'iat' => $issuedAt,
    'exp' => $expirationTime
    );
    $token_jwt = JWT::encode($payload,JWT_SECRET, "HS256");
    
    $response = $response->withHeader("Authorization", "Bearer {$token_jwt}")->withHeader("Content-Type", "application/json");
    
    $data = array('token' => $token_jwt);
    return $response->withHeader("Content-Type", "application/json")->withJson($data);
}

function authen($request, $response, $args) {
    $token = $request->getAttribute("decoded_token_data");
    return $response->withHeader("Content-Type", "application/json")->withJson($token);
}



$app = new \Slim\App; 

$app->get('/product/{id}', 'getProductById');
$app->get('/products', 'getProducts');
$app->post('/client', 'addClient');
$app->put('/client/{id}', 'updateClient');
$app->delete('/client/{id}', 'deleteClient');
$app->post('/login', 'login');
$app->get('/api/authen', 'authen');

$app->add($jwt);
$app->run(); 


